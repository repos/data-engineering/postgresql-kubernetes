# postgresql-kubernetes

This repository contains OCI image definition files (Blubberfiles) as well as their build and publish mechanism (Kokkuri) for PostgreSQL as well as the [CNPG](https://cloudnative-pg.io/documentation/1.23/container_images/) operator.

These images will be used to run PostgreSQL on Kubernetes via the CNPG operator.
